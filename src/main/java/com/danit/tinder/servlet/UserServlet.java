package com.danit.tinder.servlet;

import com.danit.tinder.dao.JdbcUserDao;
import com.danit.tinder.entity.User;
import com.danit.tinder.template.PageGenerator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

public class UserServlet extends HttpServlet{
  JdbcUserDao jbdcUserDao = new JdbcUserDao();
  List<User> users = jbdcUserDao.getAll();
  List<User> LikedUsers = new ArrayList<>();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

  resp.getWriter().write(getPage());

  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    int id = Integer.parseInt(req.getParameter("id"));

    for (User user: users) {
      if (id == user.getId()){
        if(!LikedUsers.isEmpty()){
          for (User yes: LikedUsers) {
            jbdcUserDao.saveLikedUsers(user);
            break;
          }
        }
        jbdcUserDao.saveLikedUsers(user);
        break;

      }
    }
    resp.getWriter().write(getPage());
  }

  private String getPage() {
    Random random = new Random();
    int index = random.nextInt(users.size());
    User user = users.get(index);

    PageGenerator pageGenerator = new PageGenerator();
    Map<String, Object> map = new HashMap<>();
    map.put("user", user);

    String page = pageGenerator.getPage("index.html", map);

    return page;
  }
}
