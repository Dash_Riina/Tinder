package com.danit.tinder.dao;

import com.danit.tinder.entity.User;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper {
  public User mapRow(ResultSet resultSet) throws SQLException{

    int id = resultSet.getInt("id");
    String name = resultSet.getString("name");
    int age = resultSet.getInt("age");
    String password = resultSet.getString("password");
    String src = resultSet.getString("src");


    User user = new User(id, name, age, password, src);

    user.setId(id);
    user.setName(name);
    user.setAge(age);
    user.setPassword(password);
    user.setSrc(src);

    return user;

  }
}
