package com.danit.tinder.dao;

import com.danit.tinder.entity.User;
import com.danit.tinder.service.DataSource;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JdbcUserDao {

  DataSource dataSource = new DataSource();

  public void save(User user) {

    Connection connect = dataSource.getConnection();
    try {
      PreparedStatement preparedStatement = connect.prepareStatement("INSERT INTO Irina_Tinder_Users (ID, NAME, AGE, PASSWORD, SRC) VALUES (?,?,?,?,?)");

      int userId = user.getId();
      String userName = user.getName();
      int age = user.getAge();
      String password = user.getPassword();
      String src = user.getSrc();


      preparedStatement.setInt(1, userId);
      preparedStatement.setString(2, userName);
      preparedStatement.setInt(3, age);
      preparedStatement.setString(4, password);
      preparedStatement.setString(5, src);

      preparedStatement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public List<User> getAll () {

    List<User> list = new ArrayList();
    Connection connect = dataSource.getConnection();
    try {
      Statement statement = connect.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM Irina_Tinder_Users");
      UserMapper userMapper = new UserMapper();

      while (resultSet.next()){
        list.add(userMapper.mapRow(resultSet));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }


  public List<User> getLiked () {
    List<User> list = new ArrayList<>();
    Connection connect = dataSource.getConnection();
    try {
      Statement statement = connect.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT * FROM  Tinder_Liked_Users");
      UserMapper userMapper = new UserMapper();

      while (resultSet.next()){
        list.add(userMapper.mapRow(resultSet));
      }

    } catch (SQLException e) {
      e.printStackTrace();
    }
    return list;
  }


  public void saveLikedUsers(User user) {

    Connection connect = dataSource.getConnection();
    try {
      PreparedStatement preparedStatement = connect.prepareStatement("INSERT INTO Irina_User_Like (ID_USER, ID_LIKED_USER) VALUES (?,?)");

      int currentId = 1;
      int userId = user.getId();


      preparedStatement.setInt(1, currentId);
      preparedStatement.setInt(2, userId);

      preparedStatement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

}