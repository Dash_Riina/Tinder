package com.danit.tinder.template;


import com.danit.tinder.servlet.LoginServlet;
import com.danit.tinder.servlet.MainPageServlet;
import com.danit.tinder.servlet.UserServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;


import javax.servlet.http.HttpServlet;

public class Starter extends HttpServlet {

    public static void main(String[] args) throws Exception {
      Server server = new Server(8080);
      ServletContextHandler handler = new ServletContextHandler();

      ServletHolder user = new ServletHolder(new UserServlet());
      handler.addServlet(user, "/user");

     ServletHolder liked = new ServletHolder(new LikeServlet());
     handler.addServlet(liked, "/liked");

      // ServletHolder login = new ServletHolder(new LoginServlet());
      // handler.addServlet(login, "/login");

      server.setHandler(handler);
      server.start();
      server.join();
    }
  }


