package com.danit.tinder.entity;

import com.danit.tinder.dao.JdbcUserDao;

import java.util.ArrayList;
import java.util.List;

public class AddUser {


    private List<User> users = new ArrayList<>();

    public AddUser() {
      users.add(new User( "Michaela", 25, "password1", "https://d2sh3v9axvldzm.cloudfront.net/media/photos/galleries/uploader-12902017-1511217021.jpg"));
      users.add(new User("April", 28, "password2", "https://d2sh3v9axvldzm.cloudfront.net/media/photos/galleries/uploader-13802017-1511216997.jpg"));
      users.add(new User( "Nicky", 21, "password3", "https://d2sh3v9axvldzm.cloudfront.net/media/photos/galleries/uploader-15702017-1511217018.jpg"));
      users.add(new User( "Sophia", 24, "password4", "https://d2sh3v9axvldzm.cloudfront.net/media/photos/galleries/uploader-119302017-1511216998.jpg"));
      users.add(new User( "Vanessa", 27, "password5", "https://d2sh3v9axvldzm.cloudfront.net/media/photos/galleries/SlovakRepublic_UNI17_Web_Headshot.jpg"));
      users.add(new User("Angie", 28, "password6", "https://d2sh3v9axvldzm.cloudfront.net/media/photos/galleries/uploader-15302017-1511216995.jpg"));
      users.add(new User( "Tiffany", 21, "password7", "https://d2sh3v9axvldzm.cloudfront.net/media/profiles/Malta_UNI17_Web_Headshot.jpg"));
      users.add(new User( "Frida", 23, "password8", "https://d2sh3v9axvldzm.cloudfront.net/media/profiles/Sweden_UNI17_Web_Headshot.jpg"));
      users.add(new User("Glaisy", 24, "password9", "https://d2sh3v9axvldzm.cloudfront.net/media/profiles/Bolivia_UNI17_Web_Headshot.jpg"));
      users.add(new User( "Loan", 22, "password10", "https://d2sh3v9axvldzm.cloudfront.net/media/profiles/Vietnam_UNI17_Web_Headshot.jpg"));

    }

  public List<User> getUsers() {
    return users;
  }

  public static void main(String[] args) {
    JdbcUserDao jdbcUserDao = new JdbcUserDao();
    AddUser addUser = new AddUser();
    List<User> users = addUser.getUsers();
    for (User user : users) {
      jdbcUserDao.save(user);
    }
  }

    public User auth(String name, String password) {
      for (User user : users) {
        if (user.getName().equals(name) && user.getPassword().equals(password)) {
          return user;
        }
      }
      return null;
    }

  }
