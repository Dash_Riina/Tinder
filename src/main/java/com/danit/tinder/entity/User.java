package com.danit.tinder.entity;

  public class User {
    private int id;
    private String name;
    private int age;
    private String password;
    private String src;


    public User(String name, int age, String password, String src) {
      this.name = name;
      this.age = age;
      this.password = password;
      this.src = src;
    }

    public User(int id, String name, int age, String password, String src) {
      this.id = id;
      this.name = name;
      this.age = age;
      this.password = password;
      this.src = src;
    }

    public User() {
    }

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public int getAge() {
      return age;
    }

    public void setAge(int age) {
      this.age = age;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public String getSrc() {
      return src;
    }

    public void setSrc(String src) {
      this.src = this.src;
    }

    @Override
    public String toString() {
      return "User{" +
          "id=" + id +
          ", name='" + name + '\'' +
          ", age='" + age + '\'' +
          ", password='" + password + '\'' +
          ", src='" + src + '\'' +
          '}';
    }
  }
