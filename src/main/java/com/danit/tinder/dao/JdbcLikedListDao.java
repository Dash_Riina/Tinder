package com.danit.tinder.dao;

import com.danit.tinder.entity.User;
import com.danit.tinder.service.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JdbcLikedListDao {
 DataSource dataSource = new DataSource();

  public void setLikedUser(User likedUser) {

    Connection connect = dataSource.getConnection();
    try {
      PreparedStatement preparedStatement = connect.prepareStatement("INSERT INTO Irina_User_Like (ID_USER, ID_LIKED_USER) VALUES (?,?)");

      int currentId = 1;
      int userId = likedUser.getId();

      preparedStatement.setInt(1, currentId);
      preparedStatement.setInt(2, userId);

      preparedStatement.executeUpdate();

    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
