package com.danit.tinder.servlet;

import com.danit.tinder.dao.JdbcLikedListDao;
import com.danit.tinder.dao.JdbcUserDao;
import com.danit.tinder.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LikeServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
      Integer acceptedUserId = Integer.parseInt(req.getParameter("click"));
      if(acceptedUserId != null && acceptedUserId != -1) {
//        JdbcUserDao jdbcUserDao = new JdbcUserDao();
//        User user = jdbcUserDao.getUser(acceptedUserId);
        new JdbcLikedListDao().setLikedUser(User);
      }else {
      }
      resp.sendRedirect("/user");
    }
  }
